#!/usr/bin/python3
import datetime, os, re, sys, time, urllib
import praw

body = "Hello! It looks like you forgot to share your Google Doc. To do that, click the blue 'Share' button in the top right corner of the document, then click 'Get Shareable Link.' The link you posted should then work. It is recommended that you also change 'anyone with the link *can view*' to 'anyone with the link *can comment.*' This way, people can leave line edits."
footer = "\n\n-----\n\n^(I am a bot, bleep bloop. This comment was posted automatically.) [^(Source code.)](https://gitlab.com/jwestman/yftsygd) ^(My human overlord is) [^(u/flyingpimonster)](https://reddit.com/u/flyingpimonster)^(.)"

message = body + footer
editmessage = "Edit: It looks like it's working now.\n\n~~" + body + "~~" + footer

# Create the Reddit instance
reddit = praw.Reddit("YFTSYGD")

newverified = []

def updateComment(comment):
    try:
        comment.edit(editmessage)
        print("      Reply: " + comment.id + " (updated)")
    except praw.exceptions.APIException as err:
        print("      ERROR:", err)


def replyToComment(comment):
    try:
        reply = comment.reply(message)
        print("      Reply: " + reply.id + " (new)")
    except praw.exceptions.APIException as err:
        print("      ERROR:", err)

def scanText(text):
    """ Scans text for bad Google Docs links. Returns True if one is found,
    else returns False. """
    doclinks = re.findall("https?:\/\/(?:docs|drive).google.com\/[\w/?=\-\\\\]*", text)

    for link in doclinks:
        try:
            # Remove backslashes, apparently some urls put them before dashes
            # and underscores, but we don't need them
            with urllib.request.urlopen(link.replace("\\", "")) as req:
                # check if we've been redirected to a login page, which means we
                # don't have access
                if re.search("/ServiceLogin", req.geturl()) or re.search("/signin", req.geturl()) or req.getcode() == 401:
                    return True
        except urllib.error.HTTPError as err:
            print("      ERROR:", err)
            print("      URL  :", link)

    return False


def processComment(comment):
    if comment.id in verified:
        return

    ourComment = None
    # Make sure MoreComments objects are replaced
    comment.replies.replace_more()
    for reply in comment.replies:
        if reply.author == reddit.user.me().name:
            ourComment = reply
            break

    if scanText(comment.body):
        if ourComment is None:
            # post a comment
            print("    Comment: " + comment.id)
            replyToComment(comment)
    else:
        # add this comment to the list of comments not to check
        # again
        newverified.append(comment.id)

        if ourComment is not None:
            # we already replied, make sure the comment is updated
            # because the link works now
            if not ourComment.body.startswith("Edit:"):
                print("    Comment: " + comment.id)
                updateComment(ourComment)
        else:
            print("    Comment: " + comment.id + " (ok)")


def processPostComments(post):
    """ Scans the comments of a post. """

    print("  Thread: " + post.id + " " + post.title)

    for comment in post.comments.list():
        processComment(comment)


def processPostSelf(post):
    """ Scans the self-text of a post. """

    if post.id in verified:
        return

    ourComment = None
    # Make sure MoreComments objects are replaced
    post.comments.replace_more()
    for reply in post.comments:
        if reply.author == reddit.user.me().name:
            ourComment = reply
            break

    if scanText(post.selftext):
        if ourComment is None:
            # post a comment
            print("  Post: " + post.id + " " + post.title)
            replyToComment(post)
    else:
        # add this comment to the list of comments not to check
        # again
        newverified.append(post.id)

        if ourComment is not None:
            # we already replied, make sure the comment is updated
            # because the link works now
            if not ourComment.body.startswith("Edit:"):
                print("  Post: " + post.id + " " + post.title)
                updateComment(ourComment)
        else:
            print("  Post: " + post.id + " " + post.title + " (ok)")


def processPost(post):
    """ Scans a self-text post and its comments. """

    processPostSelf(post)
    processPostComments(post)


def processSubreddit(subName):
    """ Scan the posts in a subreddit. """

    print("Subreddit: /r/" + subName)

    newPosts = reddit.subreddit(subName).new()
    week_ago = time.time() - (3600 * 168)

    for post in newPosts:
        # only scan posts up to a week old
        if post.created_utc < week_ago:
            break

        processPost(post)


def processCritiqueStickies(subName):
    """ Look for sticky threads, process ones that have "critique" in the title. """

    print("Subreddit (Critique Stickies): /r/" + subName)

    subreddit = reddit.subreddit(subName)

    stickyIndex = 1
    lastSticky = ""
    while True:
        sticky = subreddit.sticky(stickyIndex)
        if(sticky.id == lastSticky):
            break
        else:
            lastSticky = sticky.id

        if re.search("critique", sticky.title, re.IGNORECASE):
            processPostComments(sticky)
            break

        stickyIndex += 1

now = datetime.datetime.now()

# debug info
print("Running as /u/" + reddit.user.me().name + " on " + now.strftime("%Y-%m-%d %H:%M:%S"))

# clear verified.txt once a week, on Sunday (which is day 6 in python)
if (now.weekday() == 6 and now.hour == 0 and now.minute < 10):
    print("Clearing verified.txt")
    with open("verified.txt", "w"):
        pass

# Load list of already verified comments
try:
    with open("verified.txt", "r") as file:
        verified = file.read().splitlines()
except:
    verified = []

# read the command line arguments
for arg in sys.argv[1:]:
    if arg.startswith("r/"):
        # process a sub's posts
        processSubreddit(arg[2:])
    elif arg.startswith("critique:r/"):
        # process a sub's critique stickies
        processCritiqueStickies(arg[11:])
    else:
        # process a particular thread
        processPost(reddit.submission(arg))

# Write newly verified comments to verified.txt
with open("verified.txt", "a") as file:
    for item in newverified:
        file.write("%s\n" % item)

# Print a trailing newline to the output, for readability
print("")
